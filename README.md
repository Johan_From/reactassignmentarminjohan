<h1>Lost in Translation</h1>
<hr><p>An React application that uses an API for fetching and storing data than displays it for the user. The functionality that the app provides is that you enter a word of your choice than the translation in sing languages will be displayed. You can also see you 10 recent translation displayed on you profile page.</p><h2>General Information</h2>
<hr><ul>
<li>To make this app, the vscode extension live share was being used so the two authors could code together</li>
<li>The app was built with the React framework and utilizes react-router-dom and react-hook-form as libraries for handling routing and forms.</li>
<li>The app's purpose is to utilizes the technologies below in a functional matter</li>
<h2>Usages</h2>
<p>This app uses .env variables for storing sensitive information. Create two variables as described below in your .env file where the package.json file is located</p>

```
REACT_APP_API_KEY=<your key>
REACT_APP_API_URL=<your url>
```

</ul><h2>Technologies Used</h2>
<hr><ul>
<li>JavaScript</li>
</ul><ul>
<li>React</li>
</ul><ul>
<li>react-router-dom</li>
</ul><ul>
<li>react-hook-form</li>
</ul><ul>
<li>Heroku (Heroku CLI)</li>
</ul><h2>Setup</h2>
<hr><p>To get the app started follow the steps below:</p>
<p>Open prefered terminal and type in:</p>

```
$ git clone https://gitlab.com/Johan_From/reactassignmentarminjohan.git
$ cd lost_in_translation
$ npm install
$ npm start
```

<h2>Collaborators</h2>
<ul>
<li><a href="https://gitlab.com/Johan_From" target="_blank">Johan From</a></li>
<li><a href="https://gitlab.com/ArminExperis" target="_blank">Armin Ljajic</a></li>
</ul>
