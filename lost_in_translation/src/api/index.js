const apiKey = process.env.REACT_APP_API_KEY;

// Method for creating api header
export const createHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'X-API-key': apiKey
    }
}