import { createHeaders } from ".";

const apiURL = process.env.REACT_APP_API_URL;

// Methods for API calls

// Check for user
export const checkForUser = async (username) => {
    try {
        const response = await fetch(`${apiURL}/?username=${username}`)
        if(!response.ok) throw new Error('Could not complete checkForUser() request')

        const data = await response.json()
        return [null, data]
    } catch (error) {
        return [error.message, []]
    }
}

// Create user
export const createUser = async (username) => {
    try {
        const response = await fetch(apiURL, {
            method: 'POST',
            headers: createHeaders(), // Using the own headerM method
            body: JSON.stringify({
                username: username,
                translations: []

            })
        })
        if(!response.ok) throw new Error('Could not create user with username ' + username)

        const data = await response.json()
        return [null, data]
    } catch (error) {
        return [error.message, []]
    }
}

// Login User
export const loginUser = async (username) => {
    const [checkError, user] = await checkForUser(username) // Wait for the user

    if(checkError !== null){
        return [checkError, null]
    }

    if(user.length > 0){
        return [null, user.pop()]
    }

    return await createUser(username);
}   

// Find the user with the specific id
export const findUserById = async (userId) => {
    try {
        const response = await fetch(`${apiURL}/${userId}`)
        if(!response.ok){
            throw new Error('Could not fetch user with Id')
        }
        const user = await response.json()
        return [null, user]
    } catch (error) {
        return [error.message, null]
    }
}

// Add the new translation
export const addTranslation = async (user, translation) => {
    try {
        const response = await fetch(`${apiURL}/${user.id}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation] // Make shallow copy
            })
        })

        if(!response.ok){
            throw new Error('Could not update the order')
        }
        const result = await response.json();
        return [ null, result ]
         
    } catch (error) {
        return [ error.message, null ] 
    }
}

// Clear the translation history
export const translationsClearHistory = async (userId) => {
    try {
        const response = await fetch(`${apiURL}/${userId}`, {
            method: 'PATCH',
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        })
        if(!response.ok){
            throw new Error('Could not clear translations History')
        }

        const result = await response.json();
        return [ null, result ]
    } catch (error) {
        return [error.message, null]
    }
}