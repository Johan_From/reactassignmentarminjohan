import { translationsClearHistory } from "../../api/translator"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../Context/UserContext"
import { storageDelete, storageSave, storageCheck } from "../../utils/storage"

// Profile page
const ProfileActions = () => {
    const { user, setUser } = useUser()

    // Logout
    const handleLogoutClick = () => {
        if(window.confirm('Do you want to logout?')){
            storageDelete(STORAGE_KEY_USER)
            setUser(null)
        }
    }

    // Clear translation
    const handleClearTranslationClick = async () => {
        if(!window.confirm("Do you want to clear the translation history?\nThis cannot be undone.")) return;

        if(storageCheck(STORAGE_KEY_USER) !== true){
            const [ cleanError ] = await translationsClearHistory(user.id)

            if(cleanError !== null) return 

            const updatedUser = {
                ...user,
                translations: []
            }

            // Store local with new data
            storageSave(STORAGE_KEY_USER, updatedUser)
            setUser(updatedUser)
        }
        else{
            alert("There is no items in the local storage to delete!")
        }
    }

    return(  
        <div id="buttons">
            <button className="profileButtons" onClick={ handleLogoutClick }>Logout</button>
            <button className="profileButtons" onClick={ handleClearTranslationClick }>Clear history</button>
        </div>
    )
}

export default ProfileActions;