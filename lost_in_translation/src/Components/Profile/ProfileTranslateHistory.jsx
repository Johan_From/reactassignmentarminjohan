import ProfileTranslateHistoryItem from "./ProfileTranslateHistoryItem";

const ProfileTranslateHistory = ({ translations }) => {
    // Map the translation data to item
    const translationList = translations.map(
        (translation, index) => <ProfileTranslateHistoryItem key={ index + translation } translation={ translation }/>
    )

    // Method for getting the 10 most recent
    const tenMostRecent = (arr) => {
        return arr.slice(Math.max(translationList.length - 10, 1))
    }

    // Checks for length
    return(
        <section id="translationHistory">
            <h3>Your translation history</h3>
            { translationList.length === 0 && <p>You have no translate history</p> }

            { translationList.length <= 10 && translationList }
            
            { translationList.length > 10 && tenMostRecent(translationList)}


            
            <div id="infoTranslationHistory">
                <span>10 most recent</span>
            </div>
        </section>
    )
}

export default ProfileTranslateHistory