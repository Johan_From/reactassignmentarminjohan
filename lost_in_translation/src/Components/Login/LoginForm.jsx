import { useState, useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { checkForUser, loginUser } from '../../api/translator'
import { storageSave } from '../../utils/storage'
import { useNavigate } from 'react-router-dom' 
import { useUser } from '../../Context/UserContext'
import { STORAGE_KEY_USER } from '../../const/storageKeys'

// username config for form
const usernameConfig = {
    required: true,
    minLength: 2
}

const LoginForm = () => {
    // Variables
    const { register, handleSubmit, formState: { errors } } = useForm()
    const { user, setUser } = useUser();
    const navigate = useNavigate()

    const [loading, setLoading] = useState(false)
    const [apiError, setApiError] = useState(null)

    // Check the user when page loads
    useEffect(() => {
        if(user !== null){
            navigate('/translator')
        }
    }, [user, navigate])

    // Helper method for login user
    const loginUserHelper = async (username) => {
        const [error, userResponse] = await loginUser(username)
        setLoading(true)
        if(error !== null){
            setApiError(error)
        }

        if(userResponse !== null){
            storageSave(STORAGE_KEY_USER, userResponse)
            setUser(userResponse)
            setLoading(false)
        }
        setLoading(false)
    }

    // When pressed button
    const onSubmit = async ({ username }) => {
        const [checkUserError, checkUserExists] = await checkForUser(username)


        // Checks for user input
        if(checkUserExists.length === 0){
            if(window.confirm('This user does not exists!\nDo you want to create this user?') === true){
                await loginUserHelper(username)
            }
        }
        if(checkUserExists.length !== 0){
            await loginUserHelper(username)
        }

        if(checkUserError !== null){
            setApiError(checkForUser)
        }
    }


    // If there is an error
    const errorMessage = (() => {
        if(!errors.username){
            return null;
        }
        if(errors.username.type === 'required'){
            return <span>Username is required</span>
        }

        if(errors.username.type === 'minLength'){
            return <span>Username is short (min. 3)</span>
        }
    })()

    return(
        <>
        <div className="grid">
            <div className="gridA">

            <div id='loginForm'>
                <div id='robotAndInfo'>
                    <div>
                        <img id='robotImage' src="img/Logo-Hello.png" alt="robot"/>
                    </div>
                    <div id='firstPageInfo'>
                        <h2>Lost in Translation</h2>
                        <p>Get started</p>
                    </div>
                </div>
                <form id="loginInputField" onSubmit={ handleSubmit(onSubmit) }>
                    <div className="loginInputField-button-input">
                        <div>
                            <input id='loginInput'
                                    type="text" 
                                    placeholder="⌨ | What's your name?" 
                                    { ...register('username', usernameConfig) }
                            /> 
                        </div>
                        <div>
                            <input className="translateButton" type="image" src="img/arrow.png" alt='->'/>
                        </div>
                    </div>  
                    { errorMessage }
                    {/* <div>
                    </div> */}
                    { loading && <p>Logging in ...</p>}
                    { apiError && <p>{ apiError }</p> }
                    <div className="loginInputFieldFooter">

                    </div>
                </form>
            </div>
            </div>
            <div className="gridB">

            </div>
        </div>
        </>
    )
}

export default LoginForm