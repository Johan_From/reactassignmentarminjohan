const TranslationSummary = ({ translation }) => {
    // Id for specific key
    let counterId = 0;

    // Mapping out the translation signs
    return(
        <section>
            <div className="translationBox">
                <div className="translationBoxScroll">
                    <div className="translatedSigns">
                        { translation.map((t) => {
                            return <img src={ t.image } key={ t.id = counterId++ } alt="sign" width="35" />
                        }) }
                    </div>
                </div>
                <div className="translationInputField-footer">
                    <div className="translationInputField-footer-div">
                        <span id="translationBelowText">Translation</span>
                    </div>
                </div>
            </div>
        </section>
    )
}

export default TranslationSummary