import { useForm } from 'react-hook-form'

// Form for handling translation
const TranslationForm = ({ onTranslation }) => {
    const { register, handleSubmit } = useForm()
    const onSubmit = ({ translation }) => { onTranslation(translation) } // Handles the submit

    return(
        <form className="translationInputField" onSubmit={ handleSubmit( onSubmit ) }>
            {/* <label htmlFor="translation-text">Translation</label> <br /> */}
            <div>
                <input className="translationInput" type="text" { ...register('translation') } placeholder="⌨ | Hello" />
            </div>
            <div>
                <input className="translateButton" type="image" src="img/arrow.png" alt='->'/>
            </div>
            
        </form>
    )

}

export default TranslationForm