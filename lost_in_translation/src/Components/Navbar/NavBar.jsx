import { NavLink } from "react-router-dom"
import { useUser } from "../../Context/UserContext"

const NavBar = () => {
    const { user } = useUser()
    
    // Ternarys for redirects and show profile if user exists
    return(
        <nav className="navbar">
            <p id="homeLink">
                <img src="img/Logo-Hello.png" alt="helloRobot" width="55"/>
                { user === null ? <NavLink className="links" to="/">Lost in Translation</NavLink> : <NavLink className="links" to="/translator">Lost in Translation</NavLink>}
            </p>
            <p id="profileLink">
                { user === null ? "" : <NavLink className="links" to="/profile">{ user.username } <img src="img/profile.png" alt="profileImg" width="25"/> </NavLink> }
            </p>
        </nav>
    )
}

export default NavBar