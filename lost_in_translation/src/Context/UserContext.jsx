import { createContext, useContext, useState } from "react";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { storageRead } from "../utils/storage";
// Context object -> exposing
const UserContext = createContext();

// Get the userContext
export const useUser = () => {
    return useContext(UserContext) // return {user, setUser}
}

// Provider -> managing the state
const UserProvider = ({ children }) => {
    // Magic string/numbers
    const [user, setUser] = useState( storageRead( STORAGE_KEY_USER ) )

    const state = {
        user,
        setUser
    }
    return(
        <UserContext.Provider value={ state }>
            { children }
        </UserContext.Provider>
    )
}

export default UserProvider;