import UserProvider from "./UserContext"

// Method for using the AppContext
const AppContext = ({ children }) => {
    // paramteter -> ({ children }) or (props)
    return(
        <UserProvider> 
            {children}
        </UserProvider>
    )
}

export default AppContext