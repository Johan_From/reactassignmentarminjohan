import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import './App.css';
import NavBar from './Components/Navbar/NavBar';
import Login from './views/Login';
import Profile from './views/Profile';
import Translator from './views/Translator';
import NotFoundPage from './views/NotFoundPage'

function App() {
  return (
    <Router>
      <div className="App">
        <NavBar/>
        <Routes>
          <Route path="/" element={ <Login /> }/>
          <Route path="/translator" element={ <Translator /> }/>
          <Route path="/profile" element={ <Profile /> }/>
          <Route path="*" element={ <NotFoundPage/> }/>
        </Routes>
      </div>
    </Router>
  );
}

export default App;
