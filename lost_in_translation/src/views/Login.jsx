import LoginForm from "../Components/Login/LoginForm";

// View for login
const Login = () => {
    return(
        <>
            <LoginForm />
        </>
    )
}

export default Login;