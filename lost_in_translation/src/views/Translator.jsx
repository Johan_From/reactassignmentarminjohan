import { useState } from "react";
import withAuth from "../hoc/withAuth";
import { SIGNS } from '../const/signs'
import TranslationForm  from '../Components/Translator/TranslationForm'
import TranslationSummary from "../Components/Translator/TranslationsSummary";
import { useUser } from "../Context/UserContext";
import { addTranslation } from "../api/translator";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { storageSave } from "../utils/storage";

const Translator = () => {
    
    const [translation, setTranslation] = useState(null)
    const { user, setUser } = useUser()
    const imageArray = []

    //const [imageLetter, setImageLetter] = useState(null)

    const handleTranslationSubmit = async (translationText) => {

        const translation = translationText.toLowerCase();
        
        // eslint-disable-next-line no-useless-escape
        let regExp = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/;

        // The translation checks
        if(translation.length > 40){
            alert("The input can only be lower than 40 characters!")
            return
        }

        if(regExp.test(translation) === true){
            alert("The cannot contain special characters!")
            return
        }

        if(translation.length === 0){
            alert("You must enter a value before translating")
            return
        }

        // Translate
        if(translation !== null){
            // Split the string
            let splittedTranslation = translation.split('')
            
            splittedTranslation = splittedTranslation.filter(x => String(x).trim());
            
            // Set the letters in an object with images
            splittedTranslation.forEach(letter => {
                let emptyObject = {}

                SIGNS.forEach(sign => {
                    if(letter.includes(sign.name)){
                        emptyObject.image = sign.image
                        emptyObject.id = sign.id
                    }
                })
                imageArray.push(emptyObject)
                setTranslation(imageArray)
            })
        }

        // Add the error
        const [error, updatedUser] = await addTranslation(user, translation)

        // If an error
        if(error !== null){
            return;
        }

        // Set the new values
        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)
    }

    return(
        <>
            <TranslationForm onTranslation={ handleTranslationSubmit } />

            { translation && <TranslationSummary translation={ translation }/> }
        </>
    )
}

export default withAuth(Translator);