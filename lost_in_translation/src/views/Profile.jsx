import { useEffect } from "react";
import { findUserById } from "../api/translator";
import ProfileActions from "../Components/Profile/ProfileActions";
import ProfileTranslateHistory from "../Components/Profile/ProfileTranslateHistory";
import { STORAGE_KEY_USER } from "../const/storageKeys";
import { useUser } from "../Context/UserContext";
import withAuth from "../hoc/withAuth";
import { storageSave } from "../utils/storage";

const Profile = () => {
    const { user, setUser } = useUser()

    // Find the user by id and save to storage and set the user
    useEffect(() => {
        const findUser = async () => {
            const [ error, latestUser ] = await findUserById(user.id)
            if(error === null){
                storageSave(STORAGE_KEY_USER, latestUser)
                setUser(latestUser)
            }
        }

        findUser()
    }, [setUser, user.id])

    // The different components below in return()
    return(
        <div id="profilePage">
            <h1>Welcome to your profile</h1>
            <ProfileActions />
            <ProfileTranslateHistory translations={ user.translations }/>
        </div>
    )
}

export default withAuth(Profile);